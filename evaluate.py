import os
import sys
import io

correct=0
total=0
mCorrect=0
mTotal=0
bCorrect=0
bTotal=0
for i in range (0, 300):
    os.system(r"C:\Users\mzhang\Downloads\label_image.py --graph=/tmp/output_graph.pb --labels=/tmp/output_labels.txt --input_layer=Placeholder --output_layer=final_result --image=D:\ISIC-Archive-Downloader-master\ISIC-Archive-Downloader-master\Data\Images\ISIC_"+str(i).zfill(7)+".jpg")
    file = open(r"D:\ISIC-Archive-Downloader-master\ISIC-Archive-Downloader-master\Data\Descriptions\ISIC_"+str(i).zfill(7))
    ml = open(r"C:/Users/mzhang/PycharmProjects/TFClassifier/Scripts/file")
    mlResult=ml.readline()
    tumorDiagnosis=file.readline()
    while "benign_malignant" not in tumorDiagnosis:
        tumorDiagnosis=file.readline()
    if tumorDiagnosis[27:28]==mlResult[0:1]:
        correct=correct+1
        if tumorDiagnosis[27:28]=="m":
            mCorrect=mCorrect+1
        elif tumorDiagnosis[27:28]=="b":
            bCorrect=bCorrect+1

    if tumorDiagnosis[27:28]=="m":
        mTotal=mTotal+1
    elif tumorDiagnosis[27:28]=="b":
        bTotal=bTotal+1


    total=total+1;

    print("actual "+tumorDiagnosis[27:28])
    print("guess "+mlResult[0:1])
    print(r"overall% " + str(correct/total))

    if not mTotal==0:
        print(r"m% " + str(mCorrect/mTotal))
    if not bTotal == 0:
        print(r"b% " + str(bCorrect/bTotal))
    ml.close()
    file.close()
correct=correct/total
mCorrect=mCorrect/mTotal
bCorrect=bCorrect/bTotal

print(r"overall% "+str(correct))
if not mTotal == 0:
    print(r"m% "+str(mCorrect))
if not bTotal == 0:
    print(r"b% "+str(bCorrect))



